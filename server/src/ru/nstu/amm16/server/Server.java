package ru.nstu.amm16.server;

import ru.nstu.amm16.api.util.IndexedSet;
import ru.nstu.amm16.network.ClientServerConnection;
import ru.nstu.amm16.network.ConnectionStateListener;
import ru.nstu.amm16.network.ServerConstants;
import ru.nstu.amm16.network.messages.ClientList;

import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.out;

/**
 * Created by Alexandra on 05.11.2016.
 */
public class Server implements UserStateListener, ConnectionStateListener {

    private final IndexedSet<String, User> users;
    private final Set<User> readyUsers = new HashSet<>();
    private final IndexedSet<Integer, ClientConnection> connections;
    private final AtomicInteger nextConnectionId = new AtomicInteger(1);
    private final Object lock = new Object();

    public Server() {
        users = new IndexedSet<>(HashMap.class, User::getName);
        connections = new IndexedSet<>(HashMap.class, ClientConnection::getId);
    }

    public static void main(String[] args) {
        new Server().start();
    }

    private void start() {
        out.println("Starting Team2Viewer server");
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(ServerConstants.PORT);
        } catch (IOException e) {
            out.println("Failed to start server: " + e.getMessage());
            return;
        }

        users.put(new User("1", "1"));
        users.put(new User("2", "2"));

        users.forEach(user -> user.setStateListener(this));

        new Thread(this::runDiscoveryHost, "DiscoveryHost").start();

        try {
            out.println("Waiting for connections");
            while (true) {
                Socket socket = serverSocket.accept();
                out.println("Accepted connection from " + socket.getInetAddress());
                ClientConnection clientConnection =
                        new ClientConnection(nextConnectionId.getAndIncrement(), socket, this);
                clientConnection.setStateListener(this);
                clientConnection.open();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void runDiscoveryHost() {
        try {
            out.println("Starting discovery host");
            DatagramSocket socket = new DatagramSocket(ServerConstants.DISCOVERY_PORT);
            DatagramPacket packet = new DatagramPacket(new byte[0], 0);
            while (true) {
                socket.receive(packet);
                socket.send(packet);
                out.println("Discovery request from: " + socket.getInetAddress());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onOpened(ClientServerConnection connection) {
        synchronized (lock) {
            connections.put((ClientConnection) connection);
        }
    }

    @Override
    public void onClosed(ClientServerConnection connection) {
        final ClientConnection clientConnection = (ClientConnection) connection;
        synchronized (lock) {
            connections.removeValue(clientConnection);

            final User user = clientConnection.getUser();
            if (user != null) {
                user.disconnect();
            }
        }
    }

    public User findUser(String name) {
        return users.get(name);
    }

    public List<User> getReadyUsers() {
        synchronized (lock) {
            return new ArrayList<>(readyUsers);
        }
    }

    private void broadcastClientList() {
        for (User target : readyUsers) {
            final List<String> userNames = new ArrayList<>();
            for (User user : readyUsers) {
                if (user != target) {
                    userNames.add(user.getName());
                }
            }
            target.getMainConnection().send(new ClientList(userNames));
        }
    }

    @Override
    public void onConnected(User user) {
        synchronized (lock) {
            readyUsers.add(user);

            broadcastClientList();
        }
        out.println("User " + user.getName() + " connected");
    }

    @Override
    public void onDisconnected(User user) {
        synchronized (lock) {
            readyUsers.remove(user);

            broadcastClientList();
        }
        out.println("User " + user.getName() + " disconnected");
    }

    @Override
    public void onPeerConnected(User user) {
        synchronized (lock) {
            readyUsers.remove(user);

            broadcastClientList();
        }
        out.println("User " + user.getName() + " connected to " + user.getPeer().getName());
    }

    @Override
    public void onPeerDisconnected(User user) {
        synchronized (lock) {
            readyUsers.add(user);

            broadcastClientList();
        }
        out.println("User " + user.getName() + " disconnected from " + user.getPeer().getName());
    }
}
