package ru.nstu.amm16.server;

import ru.nstu.amm16.api.Message;
import ru.nstu.amm16.network.ClientServerConnection;
import ru.nstu.amm16.network.messages.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Alexandra on 05.11.2016.
 */
public class ClientConnection extends ClientServerConnection {

    private final int id;
    private final Server server;

    private volatile User user;

    public ClientConnection(int id, Socket socket, Server server) {
        super(socket);
        this.id = id;
        this.server = server;
    }

    public int getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    @Override
    protected void handleMessage(Message message) {
        if (message instanceof Login) {
            Login loginMessage = (Login) message;
            String name = loginMessage.getName();
            String password = loginMessage.getPassword();
            User user = server.findUser(name);
            if (user == null || !user.checkPassword(password)) {
                send(new LoginResponse(false, "Неправильное имя или пароль"));
            } else {
                if (user.connect(this)) {
                    this.user = user;
                    send(new LoginResponse(true, null));
                } else {
                    send(new LoginResponse(false, "Уже подключен"));
                }
            }
        } else if (message instanceof GetList) {
            final List<User> readyUsers = server.getReadyUsers();
            final List<String> userNames = new ArrayList<>();
            for (User user : readyUsers) {
                if (user != this.user) {
                    userNames.add(user.getName());
                }
            }
            send(new ClientList(userNames));
        } else if (message instanceof ConnectionRequest) {
            final String peerName = ((ConnectionRequest) message).getPeerName();
            final User peer = server.findUser(peerName);
            if (peer != null) {
                final ClientConnection peerConnection = peer.getMainConnection();
                if (peerConnection != null) {
                    user.connectPeer(peer);
                    peer.connectPeer(user);

                    peerConnection.send(new ConnectionRequest(user.getName()));
                    return;
                }
            }
            send(new ConnectionReply(false, null));
        } else if (message instanceof ConnectionReply) {
            final ConnectionReply reply = (ConnectionReply) message;
            final User peer = user.getPeer();
            if (peer != null) {
                if (!reply.isAccepted()) {
                    peer.disconnectPeer();
                    user.disconnectPeer();
                }
                final ClientConnection peerConnection = peer.getMainConnection();
                if (peerConnection != null) {
                    peerConnection.send(message);
                }
            }
        } else if (message instanceof PeerDisconnected) {
            user.disconnectPeer();
        }
    }

}
