package ru.nstu.amm16.server;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by Alexandra on 05.11.2016.
 */
public class User {

    private final String name;
    private final String password;

    private final AtomicReference<ClientState> state = new AtomicReference<>(ClientState.NOT_CONNECTED);

    private volatile ClientConnection mainConnection;
    private volatile User peer;
    private volatile UserStateListener stateListener = new UserStateListener() { };

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public ClientState getState() {
        return state.get();
    }

    public ClientConnection getMainConnection() {
        return mainConnection;
    }

    public User getPeer() {
        return peer;
    }

    public boolean checkPassword(String value) {
        return password.equals(value);
    }

    public boolean connect(ClientConnection connection) {
        boolean set = state.compareAndSet(ClientState.NOT_CONNECTED, ClientState.READY);
        if (set) {
            this.mainConnection = connection;
            stateListener.onConnected(this);
        }
        return set;
    }

    public boolean connectPeer(User peer) {
        final boolean set = state.compareAndSet(ClientState.READY, ClientState.CONNECTED);
        if (set) {
            this.peer = peer;
            stateListener.onPeerConnected(this);
        }
        return set;
    }

    public void disconnectPeer() {
        final boolean set = state.compareAndSet(ClientState.CONNECTED, ClientState.READY);
        if (set) {
            stateListener.onPeerDisconnected(this);
            peer = null;
        }
    }

    public void disconnect() {
        disconnectPeer();

        ClientConnection connection = this.mainConnection;
        if (connection != null) {
            connection.close();
        }
        mainConnection = null;
        state.set(ClientState.NOT_CONNECTED);

        stateListener.onDisconnected(this);
    }

    public void setStateListener(UserStateListener stateListener) {
        this.stateListener = Objects.requireNonNull(stateListener);
    }
}
