package ru.nstu.amm16.server;

public interface UserStateListener {

    default void onConnected(User user) { }

    default void onDisconnected(User user) { }

    default void onPeerConnected(User user) { }

    default void onPeerDisconnected(User user) { }

}
