package ru.nstu.amm16.network;

import ru.nstu.amm16.api.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public abstract class ClientServerConnection {

    private final Socket socket;
    private final BlockingQueue<Message> outputQueue = new LinkedBlockingQueue<>();

    private volatile Thread reader;
    private volatile Thread writer;
    private volatile boolean active;
    private volatile ConnectionStateListener stateListener = new ConnectionStateListener() { };

    public ClientServerConnection(Socket socket) {
        this.socket = socket;
    }

    protected abstract void handleMessage(Message message);

    public void open() {
        active = true;

        reader = new Thread(this::readingLoop);
        reader.setName("Connection.Reader");
        reader.setDaemon(true);
        reader.start();

        writer = new Thread(this::writingLoop);
        writer.setName("Connection.Writer");
        writer.setDaemon(true);
        writer.start();

        stateListener.onOpened(this);
    }

    public void close() {
        if (active) {
            active = false;

            outputQueue.clear();

            reader.interrupt();
            writer.interrupt();

            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            stateListener.onClosed(this);
        }
    }

    public void send(Message message) {
        outputQueue.add(message);
    }

    public boolean isActive() {
        return active;
    }

    public void setStateListener(ConnectionStateListener stateListener) {
        this.stateListener = Objects.requireNonNull(stateListener);
    }

    public Socket getSocket() {
        return socket;
    }

    private void readingLoop() {
        try (ObjectInputStream input = new ObjectInputStream(socket.getInputStream())) {
            while (active) {
                Message message = (Message) input.readObject();
                handleMessage(message);
            }
        } catch (IOException | ClassNotFoundException e) {
            close();
        }
    }

    private void writingLoop() {
        try (ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream())) {
            while (active) {
                Message message = outputQueue.take();
                output.writeObject(message);
                output.flush();
            }
        } catch (Exception e) {
            close();
        }
    }
}
