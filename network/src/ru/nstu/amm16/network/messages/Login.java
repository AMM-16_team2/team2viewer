package ru.nstu.amm16.network.messages;

import ru.nstu.amm16.api.Message;

/**
 * Created by Alexandra on 05.11.2016.
 */
public class Login extends Message {

    private final String name;
    private final String password;

    public Login(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }
}
