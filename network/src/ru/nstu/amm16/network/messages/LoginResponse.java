package ru.nstu.amm16.network.messages;

import ru.nstu.amm16.api.Message;

/**
 * Created by Alexandra on 05.11.2016.
 */
public class LoginResponse extends Message {

    private final boolean success;
    private final String message;

    public LoginResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
