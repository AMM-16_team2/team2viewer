package ru.nstu.amm16.network.messages;

import ru.nstu.amm16.api.Message;

import java.net.InetSocketAddress;

/**
 * Created by Alexandra on 05.11.2016.
 */
public class ConnectionReply extends Message {

    private final boolean accepted;
    private final InetSocketAddress address;

    public ConnectionReply(boolean accepted, InetSocketAddress address) {
        this.accepted = accepted;
        this.address = address;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public InetSocketAddress getAddress() {
        return address;
    }
}
