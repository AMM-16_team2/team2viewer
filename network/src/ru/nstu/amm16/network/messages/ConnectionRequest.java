package ru.nstu.amm16.network.messages;

import ru.nstu.amm16.api.Message;

/**
 * Created by Alexandra on 05.11.2016.
 */
public class ConnectionRequest extends Message {

    private final String peerName;

    public ConnectionRequest(String peerName) {
        this.peerName = peerName;
    }

    public String getPeerName() {
        return peerName;
    }

}
