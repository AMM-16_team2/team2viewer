package ru.nstu.amm16.network.messages;

import ru.nstu.amm16.api.Message;

import java.util.*;

/**
 * Created by Alexandra on 05.11.2016.
 */
public class ClientList extends Message {

    private final List<String> clients;

    public ClientList(List<String> clients) {
        this.clients = clients;
    }

    public List<String> getClients() {
        return clients;
    }
}
