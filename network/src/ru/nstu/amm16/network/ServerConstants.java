package ru.nstu.amm16.network;

/**
 * Константы, используемые совместно сервером и клиентом.
 */
public final class ServerConstants {

    public static final int PORT = 5123;
    public static final int DISCOVERY_PORT = 5122;

    private ServerConstants() {
        throw new UnsupportedOperationException();
    }

}
