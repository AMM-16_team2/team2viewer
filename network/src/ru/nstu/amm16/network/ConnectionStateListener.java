package ru.nstu.amm16.network;

public interface ConnectionStateListener {

    default void onOpened(ClientServerConnection connection) { }

    default void onClosed(ClientServerConnection connection) { }

}
