package ru.nstu.amm16.control;

import ru.nstu.amm16.api.*;
import ru.nstu.amm16.api.messages.BinaryMessage;

import java.awt.*;
import java.awt.event.InputEvent;
import java.nio.ByteBuffer;

public class ControlSlaveModule implements SlaveModule, MessageHandler {

    Robot robot;

    @Override
    public void init(SlaveContext context) {
        final Connector connector = context.getConnector();
        connector.setMessageHandler(this);
        try {
            robot  = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {

    }

    @Override
    public void handleMessage(Message message) {
        if (message instanceof BinaryMessage) {

            ByteBuffer bb = ByteBuffer.wrap(((BinaryMessage) message).getData());
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();

            int ms = bb.getInt(0);

            if( ms == 0 ){
                robot.keyPress(bb.getInt(4));
            }
            if( ms == 1 ){
                int x = (int)(d.width * bb.getFloat(4));
                int y = (int)(d.height * bb.getFloat(8));
                int button = bb.getInt(12);

                robot.mouseMove(x,y);
                if(button == 1){
                    robot.mousePress( InputEvent.BUTTON1_MASK );
                    robot.mouseRelease( InputEvent.BUTTON1_MASK );
                }
                if(button == 2) {
                    robot.mousePress(InputEvent.BUTTON2_MASK);
                    robot.mouseRelease(InputEvent.BUTTON2_MASK);
                }
                if(button == 3){
                    robot.mousePress( InputEvent.BUTTON3_MASK );
                    robot.mouseRelease( InputEvent.BUTTON3_MASK );
                }
            }
        }
    }
}
