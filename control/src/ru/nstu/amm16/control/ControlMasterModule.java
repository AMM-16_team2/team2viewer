package ru.nstu.amm16.control;

import ru.nstu.amm16.api.Connector;
import ru.nstu.amm16.api.MasterContext;
import ru.nstu.amm16.api.MasterModule;
import ru.nstu.amm16.api.messages.BinaryMessage;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.nio.ByteBuffer;

public class ControlMasterModule implements MasterModule {
    @Override
    public void init(MasterContext context) {
        final Connector connector = context.getConnector();
        final JComponent screenArea = context.getUserInterface().getScreenArea();
        screenArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                ByteBuffer bb = ByteBuffer.allocate(4*2);
                bb.putInt(0, 0);
                bb.putInt(4, e.getKeyCode());
                connector.sendMessage(new BinaryMessage(bb.array()));
            }
        });
        screenArea.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ByteBuffer bb = ByteBuffer.allocate(4*4);
                bb.putInt(0, 1);
                bb.putFloat(4,e.getX()/(float)screenArea.getWidth());
                bb.putFloat(8,e.getY()/(float)screenArea.getHeight());
                bb.putInt(12,e.getButton());
                connector.sendMessage(new BinaryMessage(bb.array()));
            }
        });
    }

    @Override
    public void destroy() {

    }
}
