package ru.nstu.amm16.screen;

import ru.nstu.amm16.api.Connector;
import ru.nstu.amm16.api.SlaveContext;
import ru.nstu.amm16.api.SlaveModule;
import ru.nstu.amm16.api.messages.BinaryMessage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ScreenSlaveModule implements SlaveModule {

    private Timer timer;
    Robot robot;

    @Override
    public void init(SlaveContext context) {
        try {
            robot  = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        final Connector connector = context.getConnector();

        Rectangle rec = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());

        timer = new Timer(30, event -> {
            BufferedImage screencapture = robot.createScreenCapture( rec );
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] imageInByte = null;
            try {
                ImageIO.write(screencapture, "png", baos);
                baos.flush();
                imageInByte = baos.toByteArray();
                baos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(imageInByte != null)
                connector.sendMessage(new BinaryMessage(imageInByte));
        });
        timer.start();
    }

    @Override
    public void destroy() {

    }
}
