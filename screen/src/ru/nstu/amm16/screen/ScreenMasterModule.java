package ru.nstu.amm16.screen;

import ru.nstu.amm16.api.*;
import ru.nstu.amm16.api.messages.BinaryMessage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ScreenMasterModule implements MasterModule, MessageHandler {

    //private Timer timer;
    BufferedImage image = null;
    MasterUi userInterface;
    @Override
    public void init(MasterContext context) {
       userInterface = context.getUserInterface();
        final Connector connector = context.getConnector();
        connector.setMessageHandler(this);
        int width = userInterface.getScreenArea().getWidth();
        int height = userInterface.getScreenArea().getHeight();
        userInterface.addScreenAreaPainter(1, graphics -> {
            if(image != null)
                graphics.drawImage(image,0,0,width,height,null);
        });
    }

    @Override
    public void destroy() {
        //timer.stop();
    }

    @Override
    public void handleMessage(Message message) {
        if (message instanceof BinaryMessage) {
            byte[] ba = ((BinaryMessage) message).getData();
            InputStream in = new ByteArrayInputStream(ba);
            try {
                image = ImageIO.read(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
            userInterface.redrawScreenArea();
        }
    }
}
