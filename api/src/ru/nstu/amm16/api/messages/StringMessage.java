package ru.nstu.amm16.api.messages;

import ru.nstu.amm16.api.Message;

/**
 * Сообщение, состоящие из одной строки.
 */
public final class StringMessage extends Message {

    private final String data;

    public StringMessage(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    @Override
    public String toString() {
        return "StringMessage{" + data + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StringMessage that = (StringMessage) o;

        return data.equals(that.data);

    }

    @Override
    public int hashCode() {
        return data.hashCode();
    }
}
