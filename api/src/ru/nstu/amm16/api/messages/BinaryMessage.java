package ru.nstu.amm16.api.messages;

import ru.nstu.amm16.api.Message;

/**
 * Сообщение, содержащее массив байт.
 */
public class BinaryMessage extends Message {

    private final byte[] data;

    public BinaryMessage(byte[] data) {
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }
}
