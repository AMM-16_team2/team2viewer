package ru.nstu.amm16.api;

/**
 * Модуль управляемого клиента.
 * Данная часть клиентского модуля запускается на стороне управляемого клиента.
 */
public interface SlaveModule {

    /**
     * Выполняет инициализацию модуля.
     * <p>
     *     Модуль управляемого клиента при инициализации может
     *     зарегистрировать обработчик сообщений методом
     *     {@link Connector#setMessageHandler(MessageHandler)}.
     * </p>
     *
     * @param context контекст управляемого клиента
     */
    void init(SlaveContext context);

    /**
     * Освобождает ресурсы, занятые модулем.
     */
    void destroy();

}
