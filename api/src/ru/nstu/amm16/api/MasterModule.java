package ru.nstu.amm16.api;

/**
 * Модуль управляющего клиента.
 * Данная часть клиентского модуля запускается на стороне управляющего клиента.
 */
public interface MasterModule {

    /**
     * Выполняет инициализацию модуля.
     * <p>
     * Модуль управляющего клиента при инициализации может сделать следующее:
     * <ul>
     *     <li>зарегистрировать обработчик сообщений методом {@link Connector#setMessageHandler(MessageHandler)}</li>
     *     <li>добавить кнопку в пользовательский интерфейс ({@link MasterUi#addButton(String, Runnable)})</li>
     *     <li>добавить обработчик перерисовки экрана
     *     ({@link MasterUi#addScreenAreaPainter(int, ScreenAreaPainter)})</li>
     *     <li>добавить обработчики событий экрана управляемого клиента ({@link MasterUi#getScreenArea()})</li>
     * </ul>
     * </p>
     *
     * @param context контекст управляемого клиента
     */
    void init(MasterContext context);

    /**
     * Освобождает ресурсы, занятые модулем.
     */
    void destroy();

}
