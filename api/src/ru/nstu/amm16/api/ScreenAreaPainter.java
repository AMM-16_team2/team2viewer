package ru.nstu.amm16.api;

import java.awt.*;

/**
 * Обработчик операции рисования на компоненте,
 * отображающем экран управляемого клиента.
 */
public interface ScreenAreaPainter {

    /**
     * Вызывается при каждой перерисовке экрана.
     * <p>
     *     Этот метод вызывается только в потоке обработки событий Swing.
     * </p>
     *
     * @param graphics    область для рисования
     */
    void paint(Graphics2D graphics);

}
