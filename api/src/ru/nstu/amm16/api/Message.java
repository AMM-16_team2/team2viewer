package ru.nstu.amm16.api;

import java.io.Serializable;

/**
 * Сообщение, которое может быть отправлено
 * между управляемым и управляющим клиентом.
 * <p>
 *     Модули могут отправлять сообщения через {@link Connector}.
 * </p>
 */
public abstract class Message implements Serializable {
}
