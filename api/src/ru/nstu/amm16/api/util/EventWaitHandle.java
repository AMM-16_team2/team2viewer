package ru.nstu.amm16.api.util;

/**
 * Позволяет ожидать наступление события.
 * Аналог {@code ManualResetEvent} и {@code AutoResetEvent} в .NET.
 */
public class EventWaitHandle {

    /** Объект синхронизации */
    private final Object sync = new Object();

    /** Значение флага */
    private volatile boolean flag;

    public EventWaitHandle() {
        this(false);
    }

    public EventWaitHandle(boolean initValue) {
        this.flag = initValue;
    }

    /**
     * Ждать, пока флаг не перейдёт в поднятое состояние.
     */
    public void waitSet() throws InterruptedException {
        waitSet(false);
    }

    /**
     * Ждать, пока флаг не перейдёт в поднятое состояние.
     * @param reset сбросить ли флаг сразу после окончания ожидания.
     */
    public void waitSet(boolean reset) throws InterruptedException {
        synchronized (sync) {
            while (!flag) {
                sync.wait();
            }
            if (reset) {
                flag = false;
            }
        }
    }

    /**
     * Поднять флаг.
     */
    public void set() {
        flag = true;
        synchronized (sync) {
            sync.notifyAll();
        }
    }

    /**
     * Сбросить флаг.
     */
    public void reset() {
        flag = false;
    }

}
