package ru.nstu.amm16.api.util;

import java.util.*;
import java.util.function.Function;

/**
 * Множество элементов, которые можно получить по ключу.
 *
 * @param <K>    тип ключа
 * @param <V>    тип значения
 */
public class IndexedSet<K, V> implements Iterable<V> {

    private final Map<K, V> map;
    private final Function<? super V, K> indexer;

    /**
     * Создаёт индексированное множество.
     *
     * @param mapClass    тип словаря ({@link Map}), используемого для хранения элементов
     * @param indexer     функция, извлекающая ключ из значения
     */
    public IndexedSet(Class<? extends Map> mapClass, Function<? super V, K> indexer) {
        try {
            @SuppressWarnings("unchecked")
            final Map<K, V> map = mapClass.<Map<K, V>>newInstance();
            this.map = map;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        this.indexer = indexer;
    }

    public V get(K key) {
        return map.get(key);
    }

    public void put(V value) {
        if (value != null) {
            map.put(indexer.apply(value), value);
        }
    }

    public boolean removeValue(V value) {
        return map.remove(indexer.apply(value)) != null;
    }

    public V remove(K key) {
        return map.remove(key);
    }

    @Override
    public Iterator<V> iterator() {
        return map.values().iterator();
    }

    public Collection<V> values() {
        return map.values();
    }

}