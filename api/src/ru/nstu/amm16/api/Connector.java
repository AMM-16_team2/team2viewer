package ru.nstu.amm16.api;

/**
 * Представляет интерфейс для отправки сообщений между
 * модулями управляемого и управляющего клиента.
 */
public interface Connector {

    /**
     * Отправляет сообщение модулю на другой стороне.
     */
    void sendMessage(Message message);

    /**
     * Устанавливает обработчик входящих сообщений,
     * отправленных на другой стороне методом {@link #sendMessage(Message)}.
     *
     * @param handler объект, обрабатывающий сообщения
     */
    void setMessageHandler(MessageHandler handler);

}
