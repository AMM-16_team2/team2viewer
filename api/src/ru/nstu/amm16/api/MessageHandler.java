package ru.nstu.amm16.api;

/**
 * Обработчик входящих сообщений.
 */
public interface MessageHandler {

    /**
     * Обработать сообщение, присланное другим клиентом.
     * <p>
     *     Этот метод вызывается в потоке обработки входящих сообщений.
     *     <br/>В нём не следует выполнять долгих операций.
     * </p>
     *
     * @param message    входящее сообщение
     */
    void handleMessage(Message message);

}
