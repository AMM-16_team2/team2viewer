package ru.nstu.amm16.api;

/**
 * Контекст управляющего клиента.
 */
public interface MasterContext extends Context {

    /**
     * Возвращает пользовательский интерфейс управляющего клиента
     * для интеграции модуля в него.
     *
     * @return объект, представляющий пользовательский интерфейс
     */
    MasterUi getUserInterface();

}
