package ru.nstu.amm16.api;

public enum ComponentState {
    INITIALIZING,
    ACTIVE,
    DESTROYED
}
