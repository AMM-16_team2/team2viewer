package ru.nstu.amm16.core;

import java.io.*;

public final class ClientConnection implements Closeable {

    private final String peerName;
    private final InputStream input;
    private final OutputStream output;

    public ClientConnection(String peerName, InputStream input, OutputStream output) {
        this.peerName = peerName;
        this.input = input;
        this.output = output;
    }

    public String getPeerName() {
        return peerName;
    }

    public InputStream getInput() {
        return input;
    }

    public OutputStream getOutput() {
        return output;
    }

    @Override
    public void close() throws IOException {
        input.close();
        output.close();
    }
}
