package ru.nstu.amm16.core.master;

import org.apache.commons.lang3.exception.ExceptionUtils;
import ru.nstu.amm16.api.ComponentState;
import ru.nstu.amm16.api.MasterModule;
import ru.nstu.amm16.api.Message;
import ru.nstu.amm16.core.*;
import ru.nstu.amm16.core.messages.Handshake;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.EOFException;
import java.util.*;

/**
 * Управляющий клиент.
 */
public class MasterClient extends ClientBase {

    private final MasterFrame mainWindow;

    private volatile Map<String, MasterModule> modules;

    public MasterClient(ClientConnection connection) {
        super(connection);

        mainWindow = new MasterFrame(connection.getPeerName());
        mainWindow.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                close();
            }
        });
    }

    @Override
    protected void startHandshake() {
        final ModuleRegistry moduleRegistry = new ModuleRegistry();
        try {
            modules = moduleRegistry.instantiateMasterModules();
        } catch (ModuleFailureException e) {
            final String message = String.format("Ошибка создания модуля %s: %s",
                    e.getModuleName(), ExceptionUtils.getStackTrace(e.getCause()));
            showError(message);
            close();
        }
        connector.send(new Parcel(null, new Handshake(modules.keySet())));
    }

    @Override
    protected void handleControlMessage(Message message) {
        if (getState() == ComponentState.INITIALIZING && message instanceof Handshake) {
            final List<String> slaveModules = ((Handshake) message).getModules();
            modules.keySet().retainAll(slaveModules);
            modules.forEach((name, module) -> {
                final ModuleContextBase context = new MasterContextImpl(name, module, this);
                context.setErrorHandler(error -> showError("Ошибка в модуле %s:\n%s\nМодуль отключен",
                        name, ExceptionUtils.getStackTrace(error)));
                contexts.put(name, context);
            });
            handshakeFinished.set();
        }
    }

    @Override
    protected void onInitializationFinished() {
        mainWindow.setVisible(true);
    }

    @Override
    protected void handleConnectionError(Exception error) {
        if (error instanceof EOFException) {
            JOptionPane.showMessageDialog(null, "Соединение закрыто", null, JOptionPane.WARNING_MESSAGE);
        } else {
            showError("Ошибка подключения: %s", error);
        }
    }

    @Override
    public void close() {
        mainWindow.dispose();
        super.close();
    }

    public MasterFrame getMainWindow() {
        return mainWindow;
    }

    void showError(String format, Object... args) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null,
                String.format(format, args), null, JOptionPane.ERROR_MESSAGE));
    }

}
