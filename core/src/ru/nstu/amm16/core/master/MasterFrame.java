package ru.nstu.amm16.core.master;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class MasterFrame extends JFrame {

    private static final int PADDING = 10;
    private static final EmptyBorder EMPTY_BORDER = new EmptyBorder(PADDING, PADDING, PADDING, PADDING);

    private final ScreenArea screenArea;
    private final JPanel buttonPanel;

    public MasterFrame(String peerName) {
        screenArea = new ScreenArea();

        final JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setBorder(EMPTY_BORDER);
        mainPanel.add(screenArea);

        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());

        final JPanel rightPanel = new JPanel(new BorderLayout());
        rightPanel.setBorder(EMPTY_BORDER);
        rightPanel.setPreferredSize(new Dimension(150, 100));
        rightPanel.add(buttonPanel, BorderLayout.NORTH);
        JButton disconnectButton = new JButton("Отключиться");
        rightPanel.add(disconnectButton, BorderLayout.SOUTH);
        disconnectButton.addActionListener(e -> dispose());

        final JPanel rootPanel = new JPanel(new BorderLayout());
        rootPanel.add(mainPanel, BorderLayout.CENTER);
        rootPanel.add(rightPanel, BorderLayout.EAST);

        setContentPane(rootPanel);
        pack();
        setSize(800, 500);
        setLocationRelativeTo(null);
        setTitle("Подключен к " + peerName + " – Team2Viewer");
    }

    public ScreenArea getScreenArea() {
        return screenArea;
    }

    public void addButton(String title, Runnable callback) {
        final JButton button = new JButton(title);
        button.addActionListener(event -> callback.run());

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1;

        buttonPanel.add(button, constraints);
        buttonPanel.add(Box.createVerticalStrut(PADDING));
    }

}
