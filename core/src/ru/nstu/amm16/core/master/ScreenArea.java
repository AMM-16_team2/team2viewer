package ru.nstu.amm16.core.master;

import ru.nstu.amm16.api.ScreenAreaPainter;

import javax.swing.*;
import java.awt.*;
import java.util.*;

public class ScreenArea extends JComponent {

    private SortedMap<Integer, ScreenAreaPainter> painters = new TreeMap<>();

    @Override
    protected void paintComponent(Graphics g) {
        final Graphics2D graphics = (Graphics2D) g;

        RenderingHints qualityHints = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        qualityHints.put(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
        graphics.setRenderingHints(qualityHints);

        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, getWidth(), getHeight());

        for (ScreenAreaPainter painter : painters.values()) {
            painter.paint(graphics);
        }
    }

    public void addPainter(ScreenAreaPainter painter, int priority) {
        final ScreenAreaPainter existingPainter = painters.get(priority);
        if (existingPainter != null) {
            throw new IllegalArgumentException(
                    "Painter with priority " + priority + " already registered: " + existingPainter);
        }
        painters.put(priority, painter);
    }

}
