package ru.nstu.amm16.core.master;

import ru.nstu.amm16.api.ComponentState;
import ru.nstu.amm16.core.ModuleContextBase;
import ru.nstu.amm16.api.*;

import javax.swing.*;
import java.awt.*;
import java.util.*;

public class MasterContextImpl extends ModuleContextBase implements MasterContext {

    private final MasterClient client;
    private final MasterModule module;

    protected MasterContextImpl(String moduleName, MasterModule module, MasterClient client) {
        super(moduleName, client.getConnector());
        this.client = client;
        this.module = module;
    }

    @Override
    protected void initModule() {
        module.init(this);
    }

    @Override
    protected void destroyModule() {
        module.destroy();
    }

    @Override
    public MasterUi getUserInterface() {
        return new MasterUiImpl();
    }

    private class MasterUiImpl implements MasterUi {

        private MasterFrame masterFrame = client.getMainWindow();

        @Override
        public void addButton(String title, Runnable callback) {
            if (getState() != ComponentState.INITIALIZING) {
                throw new IllegalStateException();
            }
            Objects.requireNonNull(callback);

            final Runnable callbackWrapper = () -> {
                if (getState() == ComponentState.ACTIVE) {
                    doModuleCode(callback);
                } else {
                    client.showError("Модуль %s отключен", getModuleName());
                }
            };
            masterFrame.addButton(title, callbackWrapper);
        }

        @Override
        public JComponent getScreenArea() {
            return client.getMainWindow().getScreenArea();
        }

        @Override
        public void addScreenAreaPainter(int priority, ScreenAreaPainter painter) {
            if (getState() != ComponentState.INITIALIZING) {
                throw new IllegalStateException();
            }

            final ScreenAreaPainter painterWrapper = graphics -> {
                if (getState() == ComponentState.ACTIVE) {
                    doModuleCode(() -> painter.paint(graphics));
                }
            };
            masterFrame.getScreenArea().addPainter(painterWrapper, priority);
        }

        @Override
        public void redrawScreenArea() {
            masterFrame.getScreenArea().repaint();
        }

        @Override
        public Window getMainWindow() {
            return masterFrame;
        }
    }
}
