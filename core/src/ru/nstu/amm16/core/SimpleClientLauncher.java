package ru.nstu.amm16.core;

import ru.nstu.amm16.core.master.MasterClient;
import ru.nstu.amm16.core.server.ClientListFrame;
import ru.nstu.amm16.core.slave.SlaveClient;

import javax.swing.*;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class SimpleClientLauncher {

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        final PipedInputStream masterInput = new PipedInputStream();
        final PipedOutputStream slaveOutput = new PipedOutputStream();
        masterInput.connect(slaveOutput);

        final PipedInputStream slaveInput = new PipedInputStream();
        final PipedOutputStream masterOutput = new PipedOutputStream();
        slaveInput.connect(masterOutput);

        final ClientConnection slaveConnection = new ClientConnection("Master", slaveInput, slaveOutput);
        final ClientConnection masterConnection = new ClientConnection("Slave", masterInput, masterOutput);

        final MasterClient masterClient = new MasterClient(masterConnection);
        final SlaveClient slaveClient = new SlaveClient(slaveConnection);

        new Thread(() -> {
            try {
                slaveClient.open(() -> System.out.println("slave closed"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                masterClient.open(() -> System.out.println("master closed"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

}
