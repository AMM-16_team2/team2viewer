package ru.nstu.amm16.core;

import ru.nstu.amm16.api.ComponentState;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

/**
 * Выполняет обмен сообщениями с другим клиентом.
 */
public final class ClientConnector {

    private final ClientConnection connection;
    private final Consumer<Parcel> inputConsumer;
    private final BlockingQueue<Parcel> outputQueue;

    private volatile ComponentState state = ComponentState.INITIALIZING;

    private Consumer<Exception> errorHandler = Throwable::printStackTrace;
    private Thread reader;
    private Thread writer;

    public ClientConnector(ClientConnection connection, Consumer<Parcel> inputConsumer) {
        this.connection = connection;
        this.inputConsumer = inputConsumer;
        this.outputQueue = new LinkedBlockingQueue<>();
    }

    public void setErrorHandler(Consumer<Exception> handler) {
        Objects.requireNonNull(handler);
        errorHandler = handler;
    }

    public void send(Parcel parcel) {
        outputQueue.add(parcel);
    }

    public void start() {
        if (state != ComponentState.INITIALIZING) {
            throw new IllegalStateException(state.name());
        }

        state = ComponentState.ACTIVE;

        reader = new Thread(this::readingLoop);
        reader.setName("ClientConnector.Reader");
        reader.setDaemon(true);
        reader.start();

        writer = new Thread(this::writingLoop);
        writer.setName("ClientConnector.Writer");
        writer.setDaemon(true);
        writer.start();
    }

    public void stop() {
        if (state != ComponentState.ACTIVE) {
            return;
        }

        state = ComponentState.DESTROYED;

        outputQueue.clear();

        reader.interrupt();
        writer.interrupt();

        try {
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Читает объекты из входного потока и отдаёт их {@link #inputConsumer}.
     */
    private void readingLoop() {
        try (ObjectInputStream input = new ObjectInputStream(connection.getInput())) {
            while (state == ComponentState.ACTIVE) {
                final Parcel incoming = (Parcel) input.readObject();
                inputConsumer.accept(incoming);
            }
        } catch (Exception e) {
            if (state == ComponentState.ACTIVE) {
                errorHandler.accept(e);
                stop();
            }
        }
    }

    private void writingLoop() {
        try (ObjectOutputStream output = new ObjectOutputStream(connection.getOutput())) {
            while (state == ComponentState.ACTIVE) {
                final Parcel nextParcel = outputQueue.take();
                output.writeObject(nextParcel);
                output.flush();
            }
        } catch (Exception e) {
            if (state == ComponentState.ACTIVE) {
                errorHandler.accept(e);
                stop();
            }
        }
    }

}
