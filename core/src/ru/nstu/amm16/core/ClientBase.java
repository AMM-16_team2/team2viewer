package ru.nstu.amm16.core;

import ru.nstu.amm16.api.ComponentState;
import ru.nstu.amm16.api.Message;
import ru.nstu.amm16.api.util.EventWaitHandle;
import ru.nstu.amm16.core.master.MasterClient;
import ru.nstu.amm16.core.slave.SlaveClient;

import javax.swing.*;
import java.io.IOException;
import java.util.*;

/**
 * Базовая реализация клиента, общая для {@link MasterClient} и {@link SlaveClient}.
 */
public abstract class ClientBase implements Client {

    protected final EventWaitHandle handshakeFinished = new EventWaitHandle();
    protected final ClientConnector connector;
    protected final Map<String, ModuleContext> contexts;

    private volatile ComponentState state = ComponentState.INITIALIZING;
    private volatile Runnable closeCallback;

    public ClientBase(ClientConnection connection) {
        connector = new ClientConnector(connection, this::handleIncomingParcel);
        connector.setErrorHandler(error -> {
            handleConnectionError(error);
            close();
        });
        contexts = new HashMap<>();
    }

    /**
     * Вызывается при получении сообщения, адресованного самому клиенту.
     */
    protected abstract void handleControlMessage(Message message);

    /**
     * Вызывается при фатальных ошибках подключения.
     * После возврата из данного метода клиент закрывается.
     */
    protected abstract void handleConnectionError(Exception error);

    /**
     * Запускает процесс подключения.
     * <p>
     *     После завершения процесса подключения необходимо выставить флаг {@link #handshakeFinished}.
     * </p>
     */
    protected abstract void startHandshake();

    /**
     * Вызывается при завершении инициализации.
     */
    protected void onInitializationFinished() { }

    @Override
    public final void open(Runnable closeCallback) throws InterruptedException {
        if (this.closeCallback != null) {
            throw new IllegalStateException();
        }

        this.closeCallback = closeCallback;

        connector.start();
        startHandshake();
        handshakeFinished.waitSet();

        if (state == ComponentState.INITIALIZING) {
            for (ModuleContext moduleContext : contexts.values()) {
                moduleContext.init();
            }

            state = ComponentState.ACTIVE;
            onInitializationFinished();
        }
    }

    @Override
    public void close() {
        if (closeCallback == null) {
            throw new IllegalStateException();
        }

        for (ModuleContext context : contexts.values()) {
            context.destroy();
        }

        connector.stop();
        state = ComponentState.DESTROYED;
        handshakeFinished.set();
        SwingUtilities.invokeLater(closeCallback);
    }

    @Override
    public final ClientConnector getConnector() {
        return connector;
    }

    @Override
    public final ComponentState getState() {
        return state;
    }

    private void handleIncomingParcel(Parcel parcel) {
        final String moduleName = parcel.getModule();
        final Message message = parcel.getMessage();
        if (moduleName != null) {
            final ModuleContext target = contexts.get(moduleName);
            if (target == null) {
                final IOException error = new IOException(
                        "Получено сообщение для неизвестного модуля " + moduleName + ": " + message);
                handleConnectionError(error);
                close();
            } else {
                target.handleMessage(message);
            }
        } else {
            handleControlMessage(message);
        }
    }

}
