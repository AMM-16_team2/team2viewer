package ru.nstu.amm16.core;

import ru.nstu.amm16.api.*;

import java.util.*;
import java.util.function.Consumer;

/**
 * Реализация контекста модуля, общая для {@link MasterContext} и {@link SlaveContext}.
 */
public abstract class ModuleContextBase implements ModuleContext {

    private final String moduleName;
    private final ClientConnector connector;
    private final List<Message> pendingMessages;

    private volatile ComponentState state = ComponentState.INITIALIZING;
    private volatile MessageHandler messageHandler;
    private volatile Consumer<Exception> errorHandler = Throwable::printStackTrace;

    protected ModuleContextBase(String moduleName, ClientConnector connector) {
        this.moduleName = moduleName;
        this.connector = connector;
        this.pendingMessages = new ArrayList<>();
    }

    /**
     * Вызвать метод инициализации у модуля.
     */
    protected abstract void initModule();

    /**
     * Вызвать метод уничтожения у модуля.
     */
    protected abstract void destroyModule();

    @Override
    public final void init() {
        final boolean initialized = doModuleCode(this::initModule);
        if (initialized) {
            state = ComponentState.ACTIVE;

            for (Message message : pendingMessages) {
                handleMessage(message);
            }
            pendingMessages.clear();
        }
    }

    @Override
    public final void destroy() {
        if (state != ComponentState.DESTROYED) {
            state = ComponentState.DESTROYED;

            doModuleCode(true, this::destroyModule);
        }
    }

    @Override
    public final void handleMessage(Message message) {
        if (state == ComponentState.INITIALIZING) {
            // Во время инициализации модуль ещё не готов принимать сообщения
            pendingMessages.add(message);
        } else if (state == ComponentState.ACTIVE) {
            final MessageHandler handler = this.messageHandler;
            if (handler != null) {
                doModuleCode(() -> handler.handleMessage(message));
            }
        }
    }

    @Override
    public final void setErrorHandler(Consumer<Exception> errorHandler) {
        Objects.requireNonNull(errorHandler);
        this.errorHandler = errorHandler;
    }

    @Override
    public final boolean doModuleCode(Runnable action) {
        return doModuleCode(false, action);
    }

    private boolean doModuleCode(boolean force, Runnable action) {
        if (state != ComponentState.DESTROYED || force) {
            try {
                action.run();

                return true;
            } catch (Exception e) {
                errorHandler.accept(e);
                destroy();
            }
        }
        return false;
    }

    @Override
    public String getModuleName() {
        return moduleName;
    }

    @Override
    public ComponentState getState() {
        return state;
    }

    @Override
    public Connector getConnector() {
        return new Connector() {
            @Override
            public void sendMessage(Message message) {
                if (state != ComponentState.DESTROYED) {
                    connector.send(new Parcel(moduleName, message));
                }
            }

            @Override
            public void setMessageHandler(MessageHandler handler) {
                ModuleContextBase.this.messageHandler = handler;
            }
        };
    }
}
