package ru.nstu.amm16.core.slave;

import ru.nstu.amm16.api.SlaveContext;
import ru.nstu.amm16.api.SlaveModule;
import ru.nstu.amm16.core.*;

public class SlaveContextImpl extends ModuleContextBase implements SlaveContext {

    private final SlaveModule module;

    protected SlaveContextImpl(String moduleName, SlaveModule module, SlaveClient client) {
        super(moduleName, client.getConnector());
        this.module = module;
    }

    @Override
    protected void initModule() {
        module.init(this);
    }

    @Override
    protected void destroyModule() {
        module.destroy();
    }
}
