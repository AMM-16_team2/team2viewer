package ru.nstu.amm16.core.slave;

import org.apache.commons.lang3.exception.ExceptionUtils;
import ru.nstu.amm16.api.ComponentState;
import ru.nstu.amm16.api.Message;
import ru.nstu.amm16.api.SlaveModule;
import ru.nstu.amm16.core.*;
import ru.nstu.amm16.core.messages.Handshake;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.EOFException;
import java.util.*;

/**
 * Управляемый клиент.
 */
public class SlaveClient extends ClientBase {

    private SlaveFrame mainWindow;

    public SlaveClient(ClientConnection connection) {
        super(connection);
        mainWindow = new SlaveFrame();
        mainWindow.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                close();
            }
        });
    }

    @Override
    protected void handleControlMessage(Message message) {
        if (getState() == ComponentState.INITIALIZING && message instanceof Handshake) {
            final List<String> moduleNames = ((Handshake) message).getModules();
            final ModuleRegistry moduleRegistry = new ModuleRegistry();
            for (String moduleName : moduleNames) {
                final SlaveModule slaveModule;
                try {
                    slaveModule = moduleRegistry.instantiateSlaveModule(moduleName);
                } catch (ModuleFailureException e) {
                    final String errorMessage = String.format("Ошибка создания модуля %s: %s",
                            e.getModuleName(), ExceptionUtils.getStackTrace(e.getCause()));
                    showError(errorMessage);

                    // Модуль с ошибкой игнорируется
                    continue;
                }

                final SlaveContextImpl context = new SlaveContextImpl(moduleName, slaveModule, this);
                context.setErrorHandler(error -> showError("Ошибка в модуле %s:\n%s\nМодуль отключен",
                        moduleName, ExceptionUtils.getStackTrace(error)));
                contexts.put(moduleName, context);
            }
            connector.send(new Parcel(null, new Handshake(contexts.keySet())));
            handshakeFinished.set();
        }
    }

    @Override
    protected void handleConnectionError(Exception error) {
        if (error instanceof EOFException) {
            JOptionPane.showMessageDialog(null, "Соединение закрыто", null, JOptionPane.WARNING_MESSAGE);
        } else {
            showError("Ошибка подключения: %s", error);
        }
    }

    @Override
    public void close() {
        mainWindow.dispose();
        super.close();
    }

    @Override
    protected void startHandshake() {
        // Подключение инициирует управляющий клиент
        // Данный клиент ждёт сообщение Handshake
        // Поэтому в данном методе делать нечего
    }

    @Override
    protected void onInitializationFinished() {
        mainWindow.setVisible(true);
    }

    void showError(String format, Object... args) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null,
                String.format(format, args), null, JOptionPane.ERROR_MESSAGE));
    }

}
