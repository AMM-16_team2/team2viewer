package ru.nstu.amm16.core;

public class ModuleFailureException extends Exception {

    private final String moduleName;

    public ModuleFailureException(String moduleName, Exception cause) {
        super("Ошибка в модуле " + moduleName, cause);
        this.moduleName = moduleName;
    }

    public String getModuleName() {
        return moduleName;
    }
}
