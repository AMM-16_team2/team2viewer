package ru.nstu.amm16.core;

import ru.nstu.amm16.api.ComponentState;
import ru.nstu.amm16.api.Context;
import ru.nstu.amm16.api.Message;

import java.util.function.Consumer;

/**
 * Внутренний интерфейс ядра для реализации контекста модуля.
 */
public interface ModuleContext extends Context {

    void init();

    void destroy();

    String getModuleName();

    ComponentState getState();

    /**
     * Обрабатывает входящее сообщение для модуля.
     *
     * @param message сообщение
     */
    void handleMessage(Message message);

    void setErrorHandler(Consumer<Exception> handler);

    /**
     * Выполняет действие от имени модуля.
     * <br/>При возникновении исключения завершает работу модуля.
     * <p>
     *     <b>Любой вызов кода модуля должен выполняться через этот метод!</b>
     * </p>
     *
     * @param action    действие, выполняемое от имени модуля
     * @return успешно ли выполнено действие
     */
    boolean doModuleCode(Runnable action);

}
