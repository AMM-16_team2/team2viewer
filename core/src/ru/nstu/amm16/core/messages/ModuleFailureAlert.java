package ru.nstu.amm16.core.messages;

import ru.nstu.amm16.api.Message;

/**
 * Сообщение об ошибке в модуле.
 */
public class ModuleFailureAlert extends Message {
}
