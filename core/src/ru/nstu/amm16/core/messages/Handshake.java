package ru.nstu.amm16.core.messages;

import ru.nstu.amm16.api.Message;

import java.util.*;

/**
 * Сообщение, устанавливающее соединение.
 * Содержит имена используемых модулей.
 */
public class Handshake extends Message {

    private List<String> modules;

    public Handshake(Collection<String> modules) {
        this.modules = new ArrayList<>(modules);
    }

    public List<String> getModules() {
        return modules;
    }

    @Override
    public String toString() {
        return "Handshake{" +
                "modules=" + modules +
                '}';
    }
}
