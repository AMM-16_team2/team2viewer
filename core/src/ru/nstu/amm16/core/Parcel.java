package ru.nstu.amm16.core;

import ru.nstu.amm16.api.Message;

import java.io.Serializable;

public class Parcel implements Serializable {

    private final String module;
    private final Message message;

    public Parcel(String module, Message message) {
        this.module = module;
        this.message = message;
    }

    public String getModule() {
        return module;
    }

    public Message getMessage() {
        return message;
    }
}
