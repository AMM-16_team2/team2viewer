package ru.nstu.amm16.core;

import ru.nstu.amm16.api.ComponentState;

public interface Client {

    /**
     * Запускает подключение.
     * Успешный возврат из метода означает завершение процесса подключения.
     *
     * @param closeCallback объект {@link Runnable}, который будет вызван при закрытии клиента;
     *                      должен быть вызван из потока обработки событий AWT
     */
    void open(Runnable closeCallback) throws InterruptedException;

    /**
     * Закрывает подключение и освобождает ресурсы.
     * Должен вызывать {@code closeCallback}, переданный в метод {@link #open(Runnable)}.
     */
    void close();

    ClientConnector getConnector();

    ComponentState getState();

}
