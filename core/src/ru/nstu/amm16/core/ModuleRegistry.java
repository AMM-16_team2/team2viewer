package ru.nstu.amm16.core;

import ru.nstu.amm16.api.MasterModule;
import ru.nstu.amm16.api.SlaveModule;
import ru.nstu.amm16.chat.ChatMasterModule;
import ru.nstu.amm16.chat.ChatSlaveModule;
import ru.nstu.amm16.control.ControlMasterModule;
import ru.nstu.amm16.control.ControlSlaveModule;
import ru.nstu.amm16.files.FilesMasterModule;
import ru.nstu.amm16.files.FilesSlaveModule;
import ru.nstu.amm16.screen.ScreenMasterModule;
import ru.nstu.amm16.screen.ScreenSlaveModule;

import java.util.*;

/**
 * Хранилище модулей. Через этот класс клиент получает все имеющиеся модули.
 */
public final class ModuleRegistry {

    private final Map<String, Class<? extends MasterModule>> masterModules = new HashMap<>();
    private final Map<String, Class<? extends SlaveModule>> slaveModules = new HashMap<>();

    public ModuleRegistry() {
        registerModule("screen", ScreenMasterModule.class, ScreenSlaveModule.class);
        registerModule("control", ControlMasterModule.class, ControlSlaveModule.class);
        registerModule("files", FilesMasterModule.class, FilesSlaveModule.class);
        registerModule("chat", ChatMasterModule.class, ChatSlaveModule.class);
    }

    /**
     * Регистрирует клиентский модуль в хранилище.
     *
     * @param name           имя (идентификатор) модуля
     * @param masterClass    класс модуля управляющего клиента
     * @param slaveClass     класс модуля управляемого клиента
     */
    private void registerModule(String name,
                                Class<? extends MasterModule> masterClass,
                                Class<? extends SlaveModule> slaveClass) {
        Objects.requireNonNull(masterClass);
        Objects.requireNonNull(slaveClass);
        if (masterModules.containsKey(name) || slaveModules.containsKey(name)) {
            throw new IllegalArgumentException("Module with name " + name + " is already registered");
        }

        masterModules.put(name, masterClass);
        slaveModules.put(name, slaveClass);
    }

    public Map<String, MasterModule> instantiateMasterModules() throws ModuleFailureException {
        final Map<String, MasterModule> modules = new HashMap<>();
        for (Map.Entry<String, Class<? extends MasterModule>> entry : masterModules.entrySet()) {
            final String name = entry.getKey();
            final Class<? extends MasterModule> clazz = entry.getValue();
            final MasterModule instance;
            try {
                instance = clazz.newInstance();
            } catch (Exception e) {
                throw new ModuleFailureException(name, e);
            }
            modules.put(name, instance);
        }
        return modules;
    }

    public SlaveModule instantiateSlaveModule(String name) throws ModuleFailureException {
        final Class<? extends SlaveModule> clazz = slaveModules.get(name);
        if (clazz != null) {
            try {
                return clazz.newInstance();
            } catch (Exception e) {
                throw new ModuleFailureException(name, e);
            }
        } else {
            throw new ModuleFailureException(name, new IllegalArgumentException("No such module"));
        }
    }

}
