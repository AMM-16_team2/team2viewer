package ru.nstu.amm16.core.server;

import ru.nstu.amm16.api.Message;
import ru.nstu.amm16.network.ClientServerConnection;

import java.net.Socket;
import java.util.function.Consumer;

public class ServerConnection extends ClientServerConnection {

    private final Consumer<Message> messageConsumer;

    public ServerConnection(Socket socket, Consumer<Message> messageConsumer) {
        super(socket);
        this.messageConsumer = messageConsumer;
    }

    @Override
    protected void handleMessage(Message message) {
        messageConsumer.accept(message);
    }
}
