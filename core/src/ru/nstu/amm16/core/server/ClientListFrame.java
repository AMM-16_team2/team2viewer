package ru.nstu.amm16.core.server;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class ClientListFrame extends JFrame {

    private final JList<String> userList = new JList<>();

    private final JLabel statusBar = new JLabel();

    private final DefaultListModel<String> listModel;
    public ClientListFrame() {
        setLayout(new BorderLayout());
        add(userList, BorderLayout.CENTER);
        add(statusBar, BorderLayout.SOUTH);

        setSize(400, 300);

        statusBar.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED), new EmptyBorder(0, 5, 0, 0)));
        statusBar.setText("Ожидание подключения");
        userList.setBorder(new CompoundBorder(
                new EmptyBorder(5, 5, 5, 5), new BevelBorder(BevelBorder.LOWERED)));

        listModel = new DefaultListModel<>();
        userList.setModel(listModel);
        setLocationByPlatform(true);
    }

    public DefaultListModel<String> getListModel() {
        return listModel;
    }

    public JList<String> getUserList() {
        return userList;
    }
}
