package ru.nstu.amm16.core.server;

import ru.nstu.amm16.api.Message;
import ru.nstu.amm16.api.util.EventWaitHandle;
import ru.nstu.amm16.core.Client;
import ru.nstu.amm16.core.ClientConnection;
import ru.nstu.amm16.core.master.MasterClient;
import ru.nstu.amm16.core.slave.SlaveClient;
import ru.nstu.amm16.network.ClientServerConnection;
import ru.nstu.amm16.network.ConnectionStateListener;
import ru.nstu.amm16.network.ServerConstants;
import ru.nstu.amm16.network.messages.*;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.*;
import java.util.*;

import static java.lang.System.out;

public class ClientLauncher implements ConnectionStateListener {

    private final EventWaitHandle connectionClosed = new EventWaitHandle();
    private final ClientListFrame clientListFrame = new ClientListFrame();

    private ServerConnection serverConnection;
    private String peerName;
    private Client client;

    public ClientLauncher() {
        clientListFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (client != null) {
                    client.close();
                }
                if (serverConnection != null) {
                    serverConnection.close();
                }
            }
        });
        final JList<String> userList = clientListFrame.getUserList();
        userList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                final String selectedValue = userList.getSelectedValue();
                if (selectedValue != null) {
                    userList.setEnabled(false);
                    peerName = selectedValue;
                    serverConnection.send(new ConnectionRequest(selectedValue));
                }
            }
        });
    }

    public void runConnection(String userName, String password,
                              Runnable onEstablished) throws IOException, InterruptedException {
        final Socket socket = new Socket();
        socket.connect(new InetSocketAddress(discoverServer(), ServerConstants.PORT));
        serverConnection = new ServerConnection(socket, this::handleMessage);
        serverConnection.setStateListener(this);
        serverConnection.open();
        serverConnection.send(new Login(userName, password));
        onEstablished.run();
        connectionClosed.waitSet();
    }

    private InetAddress discoverServer() throws IOException {
        DatagramSocket socket = new DatagramSocket();
        DatagramPacket packet = new DatagramPacket(new byte[0], 0);
        packet.setPort(ServerConstants.DISCOVERY_PORT);
        for (NetworkInterface iface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
            for (InterfaceAddress ifaceAddr : iface.getInterfaceAddresses()) {
                InetAddress addr = ifaceAddr.getBroadcast();
                if (addr != null) {
                    packet.setAddress(addr);
                    socket.send(packet);
                }
            }
        }
        socket.setSoTimeout(5_000);
        socket.receive(packet);
        return packet.getAddress();
    }

    // TODO: переместить изменение объектов Swing в главный поток
    private void handleMessage(Message message) {
        if (message instanceof LoginResponse) {
            final LoginResponse loginResponse = (LoginResponse) message;
            if (loginResponse.isSuccess()) {
                serverConnection.send(new GetList());
            } else {
                out.println("Wrong name or password");
                serverConnection.close();
            }
        } else if (message instanceof ClientList) {
            final ClientList clientList = (ClientList) message;

            final DefaultListModel<String> listModel = clientListFrame.getListModel();
            listModel.removeAllElements();
            for (String userName : clientList.getClients()) {
                listModel.addElement(userName);
            }

            clientListFrame.setVisible(true);
        } else if (message instanceof ConnectionReply) {
            final ConnectionReply reply = (ConnectionReply) message;
            if (reply.isAccepted()) {
                clientListFrame.setVisible(false);

                final InetSocketAddress address = reply.getAddress();
                final Socket socket = new Socket();
                try {
                    socket.connect(address);
                    final ClientConnection connection = new ClientConnection(
                            peerName, socket.getInputStream(), socket.getOutputStream());
                    client = new MasterClient(connection);
                    client.open(() -> {
                        serverConnection.send(new PeerDisconnected());
                        clientListFrame.setVisible(true);
                    });
                } catch (InterruptedException | IOException e) {
                    JOptionPane.showMessageDialog(clientListFrame, "Не удалось подключиться: " + e.getLocalizedMessage(),
                            "Подключение к " + peerName, JOptionPane.INFORMATION_MESSAGE);
                }
            }
            clientListFrame.getUserList().setEnabled(true);
        } else if (message instanceof ConnectionRequest) {
            final ConnectionRequest request = (ConnectionRequest) message;
            final String peerName = request.getPeerName();
            final String text = "Разрешить подключение пользователю " + peerName + "?";
            final int dialogResponse = JOptionPane.showConfirmDialog(
                    clientListFrame, text, "Входящий запрос", JOptionPane.YES_NO_OPTION);
            if (dialogResponse == JOptionPane.YES_OPTION) {
                clientListFrame.getUserList().setEnabled(false);
                clientListFrame.setVisible(false);
                try {
                    final ServerSocket serverSocket = new ServerSocket();
                    serverSocket.bind(null);
                    final InetSocketAddress address = new InetSocketAddress(
                            serverConnection.getSocket().getLocalAddress(), serverSocket.getLocalPort());
                    serverConnection.send(new ConnectionReply(true, address));
                    final Socket socket = serverSocket.accept();
                    final ClientConnection connection = new ClientConnection(
                            peerName, socket.getInputStream(), socket.getOutputStream());
                    client = new SlaveClient(connection);
                    client.open(() -> {
                        serverConnection.send(new PeerDisconnected());
                        clientListFrame.setVisible(true);
                    });
                } catch (InterruptedException | IOException e) {
                    JOptionPane.showMessageDialog(clientListFrame, "Не удалось подключиться: " + e.getLocalizedMessage(),
                            "Подключение к " + peerName, JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                serverConnection.send(new ConnectionReply(false, null));
            }
        }
    }

    @Override
    public void onClosed(ClientServerConnection connection) {
        connectionClosed.set();
        SwingUtilities.invokeLater(clientListFrame::dispose);
    }
}
