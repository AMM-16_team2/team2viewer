package ru.nstu.amm16.core.server;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class LoginFrame extends JFrame {

    private final JButton btnConnect;

    public static void main(String[] args) {
        new LoginFrame().setVisible(true);
    }

    public LoginFrame() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("Имя пользователя:"));

        JTextField txtLogin = new JTextField();
        panel.add(txtLogin);
        panel.add(new JLabel("Пароль:"));

        JPasswordField txtPassword = new JPasswordField();
        panel.add(txtPassword);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());
        panel.add(buttonPanel);

        btnConnect = new JButton("Подключиться");
        JButton btnClose = new JButton("Выход");
        btnClose.addActionListener(e -> dispose());
        btnConnect.addActionListener(e -> doConnect(txtLogin.getText(), new String(txtPassword.getPassword())));

        buttonPanel.add(btnConnect);
        buttonPanel.add(btnClose);

        setContentPane(panel);
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    private void doConnect(String login, String password) {
        btnConnect.setEnabled(false);

        new Thread(() -> {
            try {
                new ClientLauncher().runConnection(login, password, () -> SwingUtilities.invokeLater(this::dispose));
            } catch (InterruptedException | IOException e) {
                JOptionPane.showMessageDialog(this, "Ошибка подключения: " + e.getMessage(),
                        "Вход", JOptionPane.ERROR_MESSAGE);
                btnConnect.setEnabled(true);
            }
        }).start();
    }

}
