package ru.nstu.amm16.files;

import ru.nstu.amm16.api.MasterContext;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;

import ru.nstu.amm16.api.Connector;
import ru.nstu.amm16.files.messages.DataMessage;
import ru.nstu.amm16.files.messages.FileMessage;
import ru.nstu.amm16.api.messages.StringMessage;

class FileForm extends JFrame {

    private MasterContext context;
    private boolean isServer, isUser;

    private File source, currentPosition;

    private JList<File> list, list2;
    private JLabel status;

    private JPanel serverMain;
    private JScrollPane listScroller;

    private Connector connector;


    FileForm(MasterContext context) {

        this.context = context;
        connector = context.getConnector();


        source = new File(".");
        currentPosition = new File(".");

        this.isServer = this.isUser = true;
        this.setSize(400, 400);
        this.setLocation(400, 300);
        this.setTitle("Работа с файлами");
        // делаем дизайн
        this.setLayout(new BorderLayout());
        JButton btnDel = new JButton("Удалить");
        JButton btnDownload = new JButton("Скачать");
        JButton btnUpload = new JButton("Загрузить");

        btnDel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (list2.getSelectedIndex() != -1) {
                    if (!list2.getSelectedValue().isDirectory()) {
                        //uploadFile(list.getSelectedValue());
                        System.out.println("Хотим удалить" + list2.getSelectedValue().getName());
                        deleteFile(list2.getSelectedValue());
                    }
                    //else
                    //uploadDirectory(list.getSelectedValue());
                } else {
                    status.setText("Файл для удаления не выбран");
                }
            }
        });

        btnDownload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (list2.getSelectedIndex() != -1) {
                    if (!list2.getSelectedValue().isDirectory()) {
                        //uploadFile(list.getSelectedValue());
                        System.out.println("Хотим скачать" + list2.getSelectedValue().getName());
                        downloadFile(list2.getSelectedValue());
                    }
                    //else
                        //uploadDirectory(list.getSelectedValue());
                } else {
                    status.setText("Файл для скачивания не выбран");
                }
            }
        });

        btnUpload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (list.getSelectedIndex() != -1) {
                    if (!list.getSelectedValue().isDirectory())
                        uploadFile(list.getSelectedValue());
                    else
                        uploadDirectory(list.getSelectedValue());
                } else {
                    status.setText("Файл для загрузки не выбран");
                }
            }
        });
        btnDel.setEnabled(isUser);
        btnDownload.setEnabled(isUser);
        btnUpload.setEnabled(isServer);

        JPanel topMenu = new JPanel();
        topMenu.add(btnDel);
        topMenu.add(btnDownload);
        topMenu.add(btnUpload);

        status = new JLabel("Выберите фалы");
        this.add(status, BorderLayout.SOUTH);
        this.add(topMenu, BorderLayout.NORTH);

        JPanel mainPanel = new JPanel(new GridLayout(1, 2, 5, 5));

        list = new JList<>();//update(loadMyFiles(currentPosition.getPath()));
        list.setModel(update(loadMyFiles(currentPosition.getPath())));
        list.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                JList list = (JList)evt.getSource();
                if (evt.getClickCount() == 2) {
                    // Double-click detected
                    int index = list.locationToIndex(evt.getPoint());
                    //System.out.print("Клацнули на " + index + " элемент" + " sfsdfsdf ----- " + ((File)list.getSelectedValue()).getName());
                    if(((File)list.getSelectedValue()).isDirectory())
                        System.out.println("toDirectory --->>>");
                        toDirectory((File)list.getSelectedValue());

                }
            }
        });
        list.setCellRenderer(new FileRenderer(false));
        listScroller = new JScrollPane(list);
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);

        loadNotMyFiles();

        list2 = new JList<>();
        list2.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                JList list = (JList)evt.getSource();
                if (evt.getClickCount() == 2) {
                    // Double-click detected
                    int index = list.locationToIndex(evt.getPoint());
                    //System.out.print("Клацнули на " + index + " элемент" + " sfsdfsdf ----- " + ((File)list.getSelectedValue()).getName());
                    if(((File)list.getSelectedValue()).isDirectory())
                        System.out.println("toDirectory --->>>");
                        goToDirectory((File)list.getSelectedValue());
                        //toDirectory((File)list.getSelectedValue());

                }
            }
        });
        list2.setCellRenderer(new FileRenderer(false));
        list2.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        list2.setLayoutOrientation(JList.VERTICAL);
        JScrollPane listScroller2 = new JScrollPane(list2);

        serverMain = new JPanel();
        serverMain.setLayout(new BorderLayout());
        serverMain.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        JLabel name = new JLabel("Мои файлы");
        JButton updateServer = new JButton("Обновить");

        updateServer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                list.setModel(update(loadMyFiles(currentPosition.getPath())));
            }
        });

        serverMain.add(listScroller, BorderLayout.CENTER);
        serverMain.add(name, BorderLayout.NORTH);
        serverMain.add(updateServer, BorderLayout.SOUTH);


        JPanel userMain = new JPanel();
        userMain.setLayout(new BorderLayout());
        userMain.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        JLabel name2 = new JLabel("Удаленные файлы");
        JButton updateUser = new JButton("Обновить");

        updateUser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                loadNotMyFiles();
            }
        });

        userMain.add(listScroller2, BorderLayout.CENTER);
        userMain.add(name2, BorderLayout.NORTH);
        userMain.add(updateUser, BorderLayout.SOUTH);

        mainPanel.add(serverMain);
        mainPanel.add(userMain);

        this.add(mainPanel, BorderLayout.CENTER);


    }

    private void goToDirectory(File f) {
        System.out.println("Переход в удаленную директорию " + f.getPath());
        final String message = "goToDir";
        connector.sendMessage(new FileMessage(message, f));
    }

    private void toDirectory(File f) {
        System.out.println("Переход в директорию " + f.getPath());
        currentPosition = new File(currentPosition.getPath() + "\\" + f.getName());
        try {
            currentPosition = currentPosition.getCanonicalFile();
            list.setModel(update(loadMyFiles(currentPosition.getCanonicalPath() + "\\")));
        } catch (Exception e) {
            System.out.println("toDirectory");
        }

    }

    private void uploadFile (File file) {
        System.out.println("uploadFile()");
        final String message = "File";
        try {
            byte[] f = new byte[(int) file.length()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(f);
            fis.close();
            connector.sendMessage(new DataMessage(f,message, file.getName()));

        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден при записи");
        }  catch (IOException ex) {
            System.out.println("Файл не найден при записи");
        }
        System.out.print("Хотим загрузить " + file.getName());
    }

    public void saveFile(DataMessage message) {
        System.out.println("uploadFile()");
        try {
            String head = "AnswerUploadFile";
            byte[] f = ((DataMessage) message).getData();
            FileOutputStream fos = new FileOutputStream(new File(currentPosition + "\\" + ((DataMessage) message).getName()));
            fos.write(f);
            fos.close();
            list.setModel(update(loadMyFiles(currentPosition.getPath())));
            status.setText("Скачиваие файла завершено");
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        }  catch (IOException ex) {
            System.out.println("Файл не найден");
        }
    }

    private void downloadFile (File file) {
        System.out.println("downloadFile");
        String message = "getFile";
        connector.sendMessage(new FileMessage(message,file));
    }

    private void uploadDirectory (File file) {
        status.setText("Выберете файл для загрузки, а не папку");
    }

    private DefaultListModel update(File[] files) {
        DefaultListModel model = new DefaultListModel();
        model.addElement(new File(".."));
        for (File f : files) {
            model.addElement(f);
        }
        return model;
    }

    void updateClientModel (DefaultListModel<File> m) {
        list2.setModel(m);
        status.setText("Удаленные файлы обновлены");
    }
    private void deleteFile(File f) {
        System.out.println("deleteFile()");
        final String message = "delFile";
        connector.sendMessage(new FileMessage(message, f));
    }

    private void loadNotMyFiles() {
        System.out.println("loadNotMyFiles()");
        final String message = "getCurDirectoryFiles";
        connector.sendMessage(new StringMessage(message));
    }

    private File[] loadMyFiles(String curDirectory) {
        File dir = new File(curDirectory);
        File[] filesList = dir.listFiles();
        if (filesList != null)
            for (File file : filesList) {
                if (file.isFile()) {
                    System.out.println(file.getName());
                } else if (file.isDirectory()) {
                    System.out.println(file.getName() + "-----" + "Directory");
                }
            }
        return filesList;
    }


    private class FileRenderer extends DefaultListCellRenderer {

        private boolean pad;
        private Border padBorder = new EmptyBorder(3,3,3,3);

        FileRenderer(boolean pad) {
            this.pad = pad;
        }

        @Override
        public Component getListCellRendererComponent(
                JList list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus) {

            Component c = super.getListCellRendererComponent(
                    list,value,index,isSelected,cellHasFocus);
            JLabel l = (JLabel)c;
            File f = (File)value;
            l.setText(f.getName());
            l.setIcon(FileSystemView.getFileSystemView().getSystemIcon(f));
            if (pad) {
                l.setBorder(padBorder);
            }

            return l;
        }
    }

    /*
        try(Stream<Path> paths = Files.walk(Paths.get(""))) {
            System.out.print("-------------------------------------------------");
            paths.forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    System.out.println(filePath);
                }
            });
        } catch (IOException e) {
            System.out.print("LoadFiles() Exeption");
        }*/
}
