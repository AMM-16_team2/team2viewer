package ru.nstu.amm16.files;

import ru.nstu.amm16.api.*;
import ru.nstu.amm16.api.Connector;
import ru.nstu.amm16.files.messages.DataMessage;
import ru.nstu.amm16.files.messages.FileMessage;
import ru.nstu.amm16.files.messages.ModelMessage;
import ru.nstu.amm16.api.messages.StringMessage;

import javax.swing.*;
import java.io.*;


/**
 * Управляемая часть модуля передачи файлов.
 */
public class FilesSlaveModule implements SlaveModule, MessageHandler {

    Connector connector;
    Context context;

    File source, currentPosition;

    @Override
    public void init(SlaveContext context) {
        source = new File(".");
        currentPosition = new File(".");
        this.context = context;
        connector = context.getConnector();
        connector.setMessageHandler(this);

    }

    @Override
    public void destroy() {

    }

    @Override
    public void handleMessage(Message message) {
        if (message instanceof StringMessage) {
            final String text = ((StringMessage) message).getData();
            switch (text) {
                case "getCurDirectoryFiles":
                    System.out.println(text + " Отгружаем список файлов");
                    final String head = "AnswerGetCurDirectoryFiles";

                    DefaultListModel<File> result = getModel(loadFiles(currentPosition.getPath()));
                    connector.sendMessage(new ModelMessage(head, result));
                    break;
            }

        } else if (message instanceof FileMessage) {
            String head;
            switch(((FileMessage) message).getHead()) {
                case "delFile":
                    delFile(((FileMessage) message).getData());
                    head = "AnswerDelFile";
                    DefaultListModel<File> result = getModel(loadFiles(currentPosition.getPath()));
                    connector.sendMessage(new ModelMessage(head, result));
                    break;
                case "goToDir":
                    head = "AnswerGoToDir";
                    currentPosition = new File(currentPosition.getPath() + "\\" + ((FileMessage) message).getData().getName());
                    try {
                        currentPosition = currentPosition.getCanonicalFile();
                    } catch (Exception e) {
                        System.out.println("toDirectory");
                    }
                    DefaultListModel<File> result2 = getModel(loadFiles(currentPosition.getPath()));
                    connector.sendMessage(new ModelMessage(head, result2));
                    break;
                case "getFile":
                    System.out.println("uploadFile()");
                    File file = ((FileMessage) message).getData();
                    head = "File";
                    try {
                        byte[] f = new byte[(int) file.length()];
                        FileInputStream fis = new FileInputStream(file);
                        fis.read(f);
                        fis.close();
                        connector.sendMessage(new DataMessage(f,head, file.getName()));

                    } catch (FileNotFoundException e) {
                        System.out.println("Файл не найден при записи");
                    }  catch (IOException ex) {
                        System.out.println("Файл не найден при записи");
                    }
                    System.out.print("Хотим загрузить " + file.getName());
                    break;
                default:
                    head = "FileAnswerError";
                    DefaultListModel<File> result3 = getModel(loadFiles(currentPosition.getPath()));
                    connector.sendMessage(new ModelMessage(head, result3));
                    break;
            }

        } else if (message instanceof DataMessage) {
            System.out.println("uploadFile()");
            try {
                String head = "AnswerUploadFile";
                byte[] f = ((DataMessage) message).getData();
                FileOutputStream fos = new FileOutputStream(new File(currentPosition + "\\" + ((DataMessage) message).getName()));
                fos.write(f);
                fos.close();

                DefaultListModel<File> result = getModel(loadFiles(currentPosition.getPath()));
                connector.sendMessage(new ModelMessage(head, result));

            } catch (FileNotFoundException e) {
                System.out.println("Файл не найден");
            }  catch (IOException ex) {
                System.out.println("Файл не найден");
            }
        }
    }




    DefaultListModel<File> getModel(File[] files) {
        DefaultListModel<File> model = new DefaultListModel<>();
        model.addElement(new File(".."));
        if(files != null)
            for (File f : files) {
                model.addElement(f);
            }
        return model;
    }

    void delFile(File f) {
        System.out.println(f.getName());
        f.delete();
    }

    private File[] loadFiles(String curDirectory) {
        File dir = new File(curDirectory);
        File[] filesList = dir.listFiles();
        if (filesList != null)
            for (File file : filesList) {
                if (file.isFile()) {
                    System.out.println(file.getName());
                } else if (file.isDirectory()) {
                    System.out.println(file.getName() + "-----" + "Directory");
                }
            }
        return filesList;
    }
}
