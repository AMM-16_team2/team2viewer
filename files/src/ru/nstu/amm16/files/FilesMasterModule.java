package ru.nstu.amm16.files;
import ru.nstu.amm16.api.*;
import ru.nstu.amm16.files.messages.DataMessage;
import ru.nstu.amm16.files.messages.ModelMessage;

import javax.swing.*;
import java.io.File;

/**
 * Управляющая часть модуля передачи файлов.
 */
public class FilesMasterModule implements MasterModule, MessageHandler {

    private Connector connector;
    FileForm f;

    @Override
    public void init(MasterContext context) {
        final MasterUi userInterface = context.getUserInterface();
        connector = context.getConnector();
        connector.setMessageHandler(this);
        f = new FileForm(context);
        userInterface.addButton("Файлы", () -> {
            f.setVisible(true);
        });
    }

    @Override
    public void destroy() {

    }

    @Override
    public void handleMessage(Message message) {
        if (message instanceof ModelMessage) {
            //final String text = ((StringMessage) message).getData();
            DefaultListModel<File> m = ((ModelMessage) message).getData();
            f.updateClientModel(m);
        } else if (message instanceof DataMessage) {
            f.saveFile((DataMessage) message);
        }
    }
}
