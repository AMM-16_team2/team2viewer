package ru.nstu.amm16.files.messages;

import ru.nstu.amm16.api.Message;

import javax.swing.*;
import java.io.File;

/**
 * Created by demjan on 22.12.2016.
 */
public class ModelMessage extends Message {

    private final DefaultListModel<File> data;
    private final String head;

    public ModelMessage(String head, DefaultListModel data) {
        this.data = data;
        this.head = head;
    }

    public DefaultListModel getData() {
        return data;

    }

}
