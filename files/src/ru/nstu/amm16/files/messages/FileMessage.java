package ru.nstu.amm16.files.messages;

import ru.nstu.amm16.api.Message;

import javax.swing.*;
import java.io.File;

/**
 * Created by demjan on 22.12.2016.
 */

public class FileMessage extends Message {

    private final File data;
    private final String head;

    public FileMessage(String head, File data) {
        this.data = data;
        this.head = head;
    }

    public File getData() {
        return data;

    }

    public String getHead() {
        return head;
    }

}
