package ru.nstu.amm16.files.messages;

import ru.nstu.amm16.api.Message;

/**
 * Created by demjan on 22.12.2016.
 */
public class DataMessage extends Message {
    private byte[] data;
    private String head;
    private String name;

    public DataMessage(byte[] d, String head, String name) {
        data = d;
        this.head = head;
        this.name = name;
    }

    public byte[] getData() {
        return data;
    }

    public String getHead() {
        return head;
    }

    public String getName() {
        return name;
    }

}
